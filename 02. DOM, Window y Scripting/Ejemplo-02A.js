// ************ Seleccionando elementos y aplicandole propiedades *************** //
let elemento;

elemento = document.getElementById('hero'); // Obtener contenido elemento por ID
elemento = document.getElementById('hero').id; // obtener sólo el nombre del ID
elemento = document.getElementById('hero').className; // obtener la clase del elemento seleccionado por ID

elemento = document.getElementById('encabezado').textContent; // obtener el texto del elemento (opción 1)
elemento = document.getElementById('encabezado').innerText; // obtener el texto del elemento (opción 2)

encabezado = document.getElementById('encabezado');
encabezado.style.background = '#333'; // con '.style' cambiamos el 'estilo css' del elemento seleccionado.
encabezado.style.color = '#fff'; // con '.style' cambiamos el 'estilo css' del elemento seleccionado.
encabezado.style.padding = '20px'; // con '.style' cambiamos el 'estilo css' del elemento seleccionado.
encabezado.textContent = 'Los mejores cursos' // cambiar el texto del elemento seleccionado
encabezado.innerText = 'Los mejores cursos' // cambiar el texto del elemento seleccionado

// **************** Seleccionando un elemento ******************* //
// Query selector
const encabezado_1 = document.querySelector('#encabezado'); // Obtener un elemento por ID con 'querySelector'
const encabezado_2 = document.querySelector('.encabezado'); // Obtener un elemento por su 'clase' con 'querySelector'
// Aplicar CSS
encabezado_1.style.background = '#45f';
encabezado_1.style.color = '#fff'; 
encabezado_1.style.padding = '23px'; 

/* NOTA: 'getElementById()' y 'querySelector' son parecidos. La diferencia está en que 'getElementById()' 
puede solamente selecionar elementos por ID's y 'querySelector' puede seleccionar elementos por ID'S y por CLASES. */

// **************** Seleccionando múltiples elementos ******************* //
let elemento_enlaces = document.getElementsByClassName('enlace'); // Obtener contenido elemento por 'CLASE'
let enlace_1 = elemento_enlaces[0]; // obtener el 1er enlace del elemento seleccionado
let enlace_2 = elemento_enlaces[1]; // obtener el 2do enlace del elemento seleccionado

const links = document.getElementsByTagName('a'); // Seleccionar todos los enlaces del documento.
links[2].style.color = 'white'; // cambiar estilo 'CSS' de enlace seleccionado.
links[4].style.background = 'blue'; // cambiar estilo 'CSS' de enlace seleccionado.
let array_enlaces = Array.from(links); // crear un arreglo con el contenido de 'links'

let todos_elementos = document.querySelectorAll('#principal .enlace'); // Seleccionar todos los elementos según el 'ID' y la 'CLASE'
let todos_elementos = document.querySelectorAll('a'); // Seleccionar todos los elementos <a>
let todos_elementos = document.querySelectorAll('h1'); // Seleccionar todos los elementos <h1>
let todos_elementos = document.querySelectorAll('img'); // // Seleccionar todos los elementos <img>

// Seleccionar los elementos <a> que están incluidos dentro de '#principal'
let todos_elementos = document.querySelectorAll('#principal a:nth-child(odd)'); //a:nth-child: elementos <a> hijos, odd: impares

// **************** TRAVERSING ******************* //
const navegacion = document.querySelector('#principal'); // Selecciono todos los elementos contenidos por el elemento de 'id=principal'
navegacion.childNodes; // Seleccionar los elementos hijos de 'navegación', incluidos los espacios que hayan.
navegacion.children; // Seleccionar los elementos hijos de 'navegación', sin los espacios que hayan.

// Tipos de nodos
// 1 = elementos, 2 = atributos, 3 = Text Node, 8 = comentarios, 9 = documentos, 10 = doctype
navegacion.children.nodeType; // obtener el tipo de nodo. El tipo será de valor numérico
navegacion.firstElementChild // seleccionar el 1er elemento hijo
navegacion.lastElementChild; // seleccionar el último elemento hijo
navegacion.childElementCount // saber la cantidad de elementos hijos de 'navegacion'

navegacion[0].parentElement; // selecciona el elemento padre.
navegacion[4].previousElementSibling; // selecciona el elemento previo, dentro del mismo nivel.
navegacion[3].nextElementSibling; // selecciona el elemento siguiente, dentro del mismo nivel.

// ******************** CREANDO ELEMENTOS ******************* //
const nuevo_elemento = document.createElement('a'); // crear un elemento de tipo <a></a>
nuevo_elemento.className = 'enlace'; // agregamos una CLASE al nuevo elemento.
nuevo_elemento.id = 'nuevo-id'; // agregamos un ID al nuevo elemento.
nuevo_elemento.setAttribute('href', 'https://www.google.cl/'); // agregamos al atributo 'Href' una 'url'
nuevo_elemento.textContent = 'Nuevo enlace!'; // añadir texto al nuevo elemento (opción 1)

// agregar el nuevo elemento al HTML
document.querySelector('#enlaces').appendChild(nuevo_elemento);


// ******************** REEMPLAZANDO ELEMENTOS ******************* //
const nuevo_encabezado = document.createElement('h2'); // crear un elemento de tipo <h2></h2>
nuevo_encabezado.id = 'nuevo-encabezado'; // agregamos un ID al nuevo elemento.
nuevo_encabezado.appendChild(document.createTextNode('Los mejores cursos!!')); // añadir texto al nuevo elemento (opción 2)

// Seleccionar elemento anterior que será reemplazado:
const elemento_anterior = document.querySelector('#encabezado');
// Seleccionar el elemento padre:
const elemento_padre = document.querySelector('#lista-cursos');

// Reemplazado el nuevo elemento por el anterior:
elemento_padre.replaceChild(nuevo_encabezado, elemento_anterior);

// ************** Agregando y quitando CLASES y otros atributos ********************//
const remover_enlace = document.querySelectorAll('.enlaces'); // seleccionar elemento a remover
remover_enlace[0].remove(); // remover el enlace 1

const primerLi = document.querySelector('.enlace');
let elemento_A;
elemento_A = primerLi.className; // obtener el nombre de la clase
elemento_A = primerLi.classList; // obtener la clase en un DOMTokenList
elemento_A = primerLi.classList.add('nueva_clase'); // agregar una clase
elemento_A = primerLi.classList.remove('nueva-clase'); // remover una clase

primerLi.getAttribute('href'); // obtener atributo 'href'
primerLi.setAttribute('href', 'https://www.facebook.com'); // setear atributo 'href'
primerLi.setAttribute('data-id', 20); // setear un atributo
primerLi.hasAttribute('data-id'); // comprobar si existe el atributo. Devuelve un 'true' o 'false'.
primerLi.removeAttribute('data-id'); // remueve un atributo
