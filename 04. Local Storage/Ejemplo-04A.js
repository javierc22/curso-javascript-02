// Agregar a Local Storage
localStorage.setItem('nombre', 'Juan');

// Agregar a Session Storage
sessionStorage.setItem('nombre_a');

// Remover de Local Storage
localStorage.removeItem('nombre');

// Obtener datos de local storage
const dato = localStorage.getItem('nombre');
console.log('dato');

// Limpiar todo el local storage
localStorage.clear();