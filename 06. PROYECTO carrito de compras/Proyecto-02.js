// VARIABLES
const carrito = document.getElementById('carrito');
const cursos = document.getElementById('lista-cursos');
const listaCursos = document.querySelector('#lista-carrito tbody');
const vaciarCarritoBtn = document.getElementById('vaciar-carrito');

// LISTENERS
cargarEventListeners();

function cargarEventListeners() {
  // Se ejecuta cuando se presiona "Agregar Carrito"
  cursos.addEventListener('click', comprarCurso);
  // Se ejecuta cuando se presiona "eliminar" en el carrito
  carrito.addEventListener('click', eliminarCurso);
  // Se ejecuta al presionar "Vaciar Carrito"
  vaciarCarritoBtn.addEventListener('click', vaciarCarrito);
  // Al cargar el documento, mostrar el Local Storage
  document.addEventListener('DOMContentLoaded', leerLocalStorage);
}

// FUNCIONES
// Añadir el curso al carrito
function comprarCurso(e) {
  e.preventDefault();
  // Delegation para agregar al carrito
  if (e.target.classList.contains('agregar-carrito')) {
    const curso = e.target.parentElement;
    // Enviamos el curso seleccionado para tomar sus datos
    leerDatosCurso(curso);
  }
}

// Leer datos de curso seleccionado
function leerDatosCurso(curso) {
  const infoCurso = {
    // imagen: curso.querySelector('img').src, // en caso que tuviera imagen
    titulo: curso.querySelector('h6').textContent,
    precio: curso.querySelector('.precio').textContent,
    id: curso.querySelector('a').getAttribute('data-id')
  }
  
  insertarCarrito(infoCurso);
}

// Muestra el curso seleccionado en el Carrito
function insertarCarrito(curso) {
  const row = document.createElement('tr');
  row.innerHTML = `
    <td>${curso.titulo}</td>
    <td>${curso.precio}</td>
    <td><a href="#" class="borrar-curso close" data-id="${curso.id}">X </a></td>
  `;

  listaCursos.appendChild(row);
  guardarCursoLocalStorage(curso);
}

// Elimina el curso del carrito en el DOM
function eliminarCurso(e) {
  e.preventDefault();
  let curso, cursoId;
  if (e.target.classList.contains('borrar-curso')) {
    e.target.parentElement.parentElement.remove();
    curso = e.target.parentElement.parentElement;
    cursoId = curso.querySelector('a').getAttribute('data-id');
  }

  eliminarCursoLocalStorage(cursoId);
}

// Elimina los cursos del carrito en el DOM
function vaciarCarrito(params) {
  while(listaCursos.firstChild){
    listaCursos.removeChild(listaCursos.firstChild);
  }

  // Vaciar Local Storage
  vaciarLocalStorage();
  // Sirve para evitar un salto en la vista del navegador. Ver video 'Vaciando carrito con un botón'
  return false;
}

// Almacena curso del carrito en Local Storage
function guardarCursoLocalStorage(curso) {
  let cursos;
  // Toma el valor de un arreglo con datos de Local Storage o vacío
  cursos = obtenerCursosLocalStorage();
  // El curso seleccionado se agrega al arreglo
  cursos.push(curso);
  // Guardo los datos del arreglo a Local Storage y convierto el arreglo a un valor "string"
  localStorage.setItem('cursos', JSON.stringify(cursos)); 
}

// Comprueba que haya elementos en Local Storage
function obtenerCursosLocalStorage() {
  let cursosLocalStorage;
  // Comprobamos si hay algo en Local Storage
  if (localStorage.getItem('cursos') === null) {
    cursosLocalStorage = [];
  } else{
    cursosLocalStorage = JSON.parse(localStorage.getItem('cursos'));
  }

  return cursosLocalStorage;
}

// Imprime los cursos de Local Storage en el carrito
function leerLocalStorage() {
  let cursosLocalStorage;
  cursosLocalStorage = obtenerCursosLocalStorage();
  cursosLocalStorage.forEach( function (curso) {
    // Construir Template
    const row = document.createElement('tr');
    row.innerHTML = `
      <td>${curso.titulo}</td>
      <td>${curso.precio}</td>
      <td><a href="#" class="borrar-curso close" data-id="${curso.id}">X </a></td>
    `;
  
    listaCursos.appendChild(row);
  });
}

// Elimina el curso por ID de Local Storage
function eliminarCursoLocalStorage(curso) {
  let cursosLocalStorage;
  // Obtenemos el arreglo de cursos de local storage
  cursosLocalStorage = obtenerCursosLocalStorage();
  // Iteramos comparando el ID del curso borrado con los de Local Storage
  cursosLocalStorage.forEach( function (cursoLS, index) {
    if (cursoLS.id === curso) {
      cursosLocalStorage.splice(index, 1);
    }
  });
  // Añadimos el arreglo de cursos actualizado a Local Storage
  localStorage.setItem('cursos', JSON.stringify(cursosLocalStorage));
}

// Eliminar todos los cursos de Local Storage
function vaciarLocalStorage() {
  localStorage.clear();
}