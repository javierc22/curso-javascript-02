// Variables
const email = document.getElementById('email');
const asunto = document.getElementById('asunto');
const mensaje = document.getElementById('mensaje');
const BtnEnviar = document.getElementById('enviarBtn');
const formularioEnviar = document.getElementById('enviar-email');
const BtnReset = document.getElementById('resetBtn');

// Event Listener
eventListeners();

function eventListeners() {
  // Inicio de la aplicación y deshabilitar Submit
  document.addEventListener('DOMContentLoaded', inicioApp);
  // Campos del formulario
  email.addEventListener('blur', validarCampo);
  asunto.addEventListener('blur', validarCampo);
  mensaje.addEventListener('blur', validarCampo);
  // Enviar el email
  formularioEnviar.addEventListener('submit', enviarEmail);
  // Botón de reset
  BtnReset.addEventListener('click', resetFormulario);
}

// Funciones
function inicioApp() {
  // Deshabilitar el envío
  BtnEnviar.disabled = true;
}

// Valida que el campo tenga algo escrito
function validarCampo() {
  // Se valida la longitud del texto y que no esté vacío
  validarLongitud(this);
  // Validar únicamente el email
  if (this.type === 'email') {
    validarEmail(this);
  }

  let errores = document.querySelectorAll('.error');

  if (email.value !== '' && asunto.value !== '' && mensaje.value !== '') {
    if (errores.length === 0) {
      BtnEnviar.disabled = false;
    } else{
      BtnEnviar.disabled = true;
    }
  }
}
// Resetear el Formulario
function resetFormulario(e) {
  formularioEnviar.reset();
  e.preventDefault();
}

// Se ejecuta cuando se envía el correo
function enviarEmail(e) {
  // Spinner al presionar Enviar
  const spinnerGif = document.querySelector('#spinner');
  spinnerGif.style.display = 'block';
  // Gif que envía email
  const enviado = document.createElement('img');
  enviado.src = 'enviado.gif';
  enviado.style.display = 'block';
  // Ocultar Spinner y mostrar Gif de enviado
  setTimeout(function () {
    spinnerGif.style.display = 'none';
    document.querySelector('#loaders').appendChild(enviado);

    setTimeout(function () {
      enviado.remove();
      formularioEnviar.reset();
    }, 4000);

  },2000);
}
// Verifica la longitud de los campos
function validarLongitud(campo) {
  if (campo.value.length > 0) {
    campo.classList.add('is-valid');
    campo.classList.remove('is-invalid')
    campo.classList.remove('error');
  } else{
    campo.classList.add('is-invalid');
    campo.classList.add('error');
    campo.classList.remove('is-valid');
  }
}

function validarEmail(campo) {
  const mensaje = campo.value;
  if (mensaje.indexOf('@') != -1) {
    campo.classList.add('is-valid');
    campo.classList.remove('is-invalid')
    campo.classList.remove('error');
  } else{
    campo.classList.add('is-invalid');
    campo.classList.add('error');
    campo.classList.remove('is-valid');
  }
}