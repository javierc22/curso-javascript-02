const numero1 = 30,
      numero2 = 20,
      numero3 = 20.20,
      numero4 = .1020,
      numero5 = -3;

// ---- Suma ---- //
let resultado_1 = numero1 + numero2;
// ---- Resta ---- //
let resultado_2 = numero1 - numero2;
// ---- Multiplicación ---- //
let resultado_3 = numero1 * 2;
// ---- División ---- //
let resultado_4 = numero1 / numero2;
// ---- Módulo ---- //
let resultado_5 = numero1 % numero2;
// ---- Pi ---- //
let resultado_6 = Math.PI;
// ---- Redondeo ---- //
let resultado_7 = Math.round(2.5);
// ---- Redondeo hacia arriba ---- //
let resultado_8 = Math.ceil(2.2);
// ---- Redondeo hacia abajo ---- //
let resultado_9 = Math.floor(2.8);
// ---- Raíz cuadrada ---- //
let resultado_10 = Math.sqrt(144);
// ---- Valor absoluto ---- //
let resultado_11 = Math.abs(-3);
// ---- Potencia ---- //
let resultado_12 = Math.pow(8, 2);
// ---- Número aleatorio ---- //
let resultado_13 = Math.random();
// ---- Incrementos y decrementos ---- //
let puntaje = 10;
let resultado_14 = puntaje += 3;
let resultado_15 = puntaje -= 3;

console.log(resultado_1);
console.log(resultado_2);
console.log(resultado_3);
console.log(resultado_4);
console.log(resultado_5);
console.log(resultado_6);
console.log(resultado_7);
console.log(resultado_8);
console.log(resultado_9);
console.log(resultado_10);
console.log(resultado_11);
console.log(resultado_12);
console.log(resultado_13);
console.log(puntaje);
console.log(resultado_14);
console.log(resultado_15);