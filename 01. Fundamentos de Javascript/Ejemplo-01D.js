// ************* TIPOS DE DATOS ************//
// ---- String (cadena de texto) ---- //
let valor_1 = 'Juan';
let valor_2 = "20";
// ---- Números ---- //
let valor_3 = 20;
let valor_4 = 20.20;
let valor_5 = -30;
// ---- Booleano ---- //
let valor_6 = true;
let valor_7 = false;
// ---- Null ---- //
let valor_8 = null;
// ---- symbol ---- //
let valor_9 = Symbol('simbolo');
// ---- array (arreglo) ---- //
let valor_10 = [1,2,3,4];
// ---- Objetos ---- //
let valor_11 = {nombre:'Juan', profesion:'programador'};
// ---- Fecha (Date) ---- //
let valor_12 = new Date();

console.log(typeof valor_1); // 'typeof' muestra el tipo de dato


// ************* OPERADORES DE COMPARACIÓN ************//
console.log(20 > 10); // true
console.log(20 < 10); // false
console.log(20 == '20'); // comparador no tan estricto, sólo compara el valor. Devuelve 'true'
console.log(20 === '20'); // comparador estricto, compara el valor y tipo de dato. Devuelve 'False'
console.log(2 != 3); // true
console.log(2 != 2); // false

// ************* CONVERTIR STRINGS A NUMEROS ************//
console.log(Number("50"));
console.log(parseInt("90"));
console.log(Number('tres')); // Regresa un 'NaN' (Not At Number);
console.log(parseFloat('100'));
console.log(parseInt('102.2365'));

const dato_1 = 189.1356875;
console.log(parseFloat(dato_1).toFixed(2)); // valor tipo 'float' con 2 decimales

// ************* CONVERTIR NUMEROS a STRING ************//
console.log(String(450)); // Número a string
console.log(String(true)); // booleano a string
console.log(String([1,2,3])); // array a string

let a = 12;
let b = a.toString(); 