// ************* ARREGLOS Y SUS METODOS **************//
// ---- arreglo numérico ---- //
const numeros = [10,20,30,40,50,60];
// ---- arreglo de String (método alternativo) ---- //
const meses = new Array('Abril','Mayo', 'Junio', 'Julio', 'Agosto','Septiembre');
// ---- arreglo mezclado ---- //
const array_mezclado = ['Hola', 20, true, null, false, undefined];
console.log(array_mezclado);

// ---- Longitud arreglo 'meses'  ---- //
console.log(meses.length);
// ---- Saber si es o no un arreglo ---- //
console.log(Array.isArray(meses)); // Devuelve 'true'
// ---- seleccionar un elemento del arreglo ---- //
console.log(meses[3]);
// ---- Insertar un valor al arreglo ---- //
meses.push('Diciembre!');
console.log(meses);
// ---- Insertar un valor al principio del arreglo ---- //
meses.unshift('Valor al principio! :v');
console.log(meses);
// ---- Enontrar el índice de un elemento en el arreglo --- //
console.log(meses.indexOf('Julio'));
// ---- Eliminar un elemento del arreglo ---- //
meses.pop();
console.log(meses);
// ---- Eliminar un elemento al principio del arreglo ---- //
meses.shift();
console.log(meses);
// ---- Quitar un rango de elementos ---- //
meses.splice(1,3); // Elimina el índice '1' y 3 elementos más a partir del índice '1'
console.log(meses);
// ---- Invertir el orden del arreglo ---- //
meses.reverse();
console.log(meses);
// ---- Unir un arreglo con otro ---- //
let arreglo_1 = [1,2,3,4], 
    arreglo_2 = [5,6,7,8];
console.log(arreglo_1.concat(arreglo_2));
// ---- Ordenar un arreglo alfabéticamente ---- //
const frutas = ['Plátano', 'Manzana', 'Fresa', 'Naranja', 'Sandía', 'Mora'];
console.log(frutas.sort());
// ---- Ordenar un arreglo de números ---- //
let arreglo_3 = [3,1,9,32,54,10,45,85,78,7,12];
arreglo_3.sort(function (x,y) {
  return x - y;
});
console.log(arreglo_3);