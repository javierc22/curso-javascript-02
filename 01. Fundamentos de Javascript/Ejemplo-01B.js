let aprendiendo = 'aprendiendo', tecnologia = 'javascript';
let mensaje = 'Aprendiendo Javascript moderno, html, css'

console.log(tecnologia.length); // largo del string
console.log(`${aprendiendo} ${tecnologia}`); // concatenación
console.log(tecnologia.concat(' es genial!')); // concatenación
console.log(tecnologia.toUpperCase()); // convertir string a mayúsculas
console.log(tecnologia.toLowerCase()); // convertir string a minúsculas
console.log(mensaje.indexOf('Javascript')); // encuentra la posición donde empieza 'Javascript'
console.log(mensaje.substring(0,11)); // muestra sólo desde los indices 0 al 11 de la cadena de texto (string)
console.log(mensaje.split(', ')); // separa el 'string' por ', ' (comas)
console.log(mensaje.replace('css', 'php')); // reemplaza 'css' por 'php'
console.log(mensaje.includes('css')); // Busca un valor dentro de una cadena de texto. Verifica si en este caso 'css' está incluida en 'mensaje'
console.log(tecnologia.repeat(5)); // repite el string 'tecnología' 5 veces 