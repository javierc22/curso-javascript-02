// ***************************** FECHAS **************************************//
// En javascript existe un objeto llamado Date.
const diaHoy= new Date();
console.log(diaHoy);
// Fecha en específico 'Mes-día-año'
const navidad2017 = new Date('12-25-2017');
console.log(navidad2017);

const dia = new Date();
// Mes
let valor_1 = dia.getMonth();
// Día
let valor_2 = dia.getDate();
let valor_3 = dia.getDay();
// Año
let valor_4 = dia.getFullYear();
// Minutos
let valor_5 = dia.getMinutes();
// Hora
let valor_6 = dia.getHours();
// Milisegundos
let valor_7 = dia.getMilliseconds();
// Milisegundos desde 1970
let valor_8 = dia.getTime();
// Cambiar a un año diferente (setear un año diferente)
let valor_9 = dia.setFullYear(2016);
let valor_10 = dia.getFullYear();

console.log(valor_10);

// ************** ESTRUCTURAS DE CONTROL IF, ELSE, ELSE IF ******************* //
const edad = 18;

if (edad == 18) {
  console.log('Si puedes entrar al sitio!')
} else {
  console.log('No Si puedes entrar al sitio!')
}

if (edad >= 18) {
  console.log('Si puedes entrar al sitio!')
} else {
  console.log('No Si puedes entrar al sitio!')
}

if (edad <= 18) {
  console.log('Si puedes entrar al sitio!')
} else {
  console.log('No Si puedes entrar al sitio!')
}

if (edad != 18) {
  console.log('Si puedes entrar al sitio!')
} else {
  console.log('No Si puedes entrar al sitio!')
}

// Comprobar si una variable tiene un valor
let puntaje;

if (puntaje) {
  console.log(`El puntaje fue de ${puntaje}`);
} else {
  console.log('No hay puntaje!');
}

if (typeof puntaje != 'undefined') {
  console.log(`El puntaje fue de ${puntaje}`);
} else {
  console.log('No hay puntaje!');
}

// Ejemplo del carrito
let efectivo = 500;
let totalCarrito = 800;

if (efectivo > totalCarrito) {
  console.log('Pago correcto.');
} else{
  console.log('Fondos insuficientes :(');
}

// else if - operador &&
let hora = 12;

if (hora > 0 && hora <= 10) {
  console.log('Buenos días!');
} else if(hora > 10 && hora <= 18) {
 console.log('Buenas Tardes');
} else if(hora > 18 && hora <= 24){
  console.log('Buenas noches');
} else{
  console.log('Hora no válida');
}

let puntaje_2 = 100;

if (puntaje_2 < 150) {
  console.log('Puntaje < 150');
} else if(puntaje_2 < 200){
  console.log('Puntaje < 200');
}

// operador ||
let dineroEfectivo = 300, 
    credito = 700,
    totalCompra = 500;

if (totalCompra < efectivo || totalCompra < credito) {
  console.log('Puedo pagar :)');
} else {
  console.log('No puedo pagar :(');
}

// Ternario
const logueado = true;
console.log( logueado === true ? 'Si se logueo' : 'No se logueo');

// ************************** Switch ************************************** //
const metodoPago = 'tarjeta';

// Ejemplo 1
switch (metodoPago) {
  case 'efectivo':
    console.log(`El usuario pagó con ${metodoPago}`);
    break;
  case 'cheque':
    console.log(`El usuario pagó con ${metodoPago}`);
    break;
  case 'tarjeta':
    console.log(`El usuario pagó con ${metodoPago}`);
    break;
  default:
  console.log('Método de pago no soportado');
    break;
}

// Ejemplo 2
let mes;
switch (new Date().getMonth()) {
  case 0:
    mes = 'Enero';
    break;
  case 1:
    mes = 'Febrero';
    break;
  case 2:
    mes = 'Marzo';
    break;
  case 3:
    mes = 'Abril';
    break;
  case 4:
    mes = 'Mayo';
    break;
  case 5:
    mes = 'Junio';
    break;
  case 6:
    mes = 'Julio';
    break;
  case 7:
    mes = 'Agosto';
    break;
  case 8:
    mes = 'Septiembre';
    break;
  case 9:
    mes = 'Octubre';
    break;
  case 10:
    mes = 'Noviembre';
    break;  
  case 11:
    mes = 'Diciembre';
    break;
}

console.log(`Este mes es ${mes}`);

// *********************** ciclo FOR (For loop) ********************** //

// Ejemplo 1
for (let i = 0; i < 10; i++) {
  console.log(`Número: ${i}`);
}

// Ejemplo 2
for (let i = 0; i < 10; i++) {
  if(i == 5){
    console.log('Voy en el 5');
    break;
  }
  console.log(`Ahora ${i}`);
}

for (let i = 0; i < 10; i++) {
  if(i == 5){
    console.log('Voy en el 5');
    continue;
  }
  console.log(`Ahora ${i}`);
}

// Ejemplo 3
for (let i = 0; i< 10; i++) {
  if(i % 2 == 0){
    console.log(`El número ${i} es par.`);
  } else {
    console.log(`El número ${i} es impar`);
  }
}

// Ejemplo 4
const arrayProductos = ['camisa', 'boleto', 'guitarra', 'disco'];
for (let i = 0; i < arrayProductos.length; i++) {
  console.log(`Tu producto ${arrayProductos[i]} fue agregado!`)
}