// *********************** ciclo WHILE y DO WHILE (While loop / Do while loop) ********************** //

// Ejemplo 1
let i = 0;
while (i < 10) {
  console.log(`Número: ${i}`);
  i++;
}

// Ejemplo 2
let a = 0;
while (a < 10) {
  if(a === 5){
    console.log('Voy en el 5');
    a++;
    break;
  }
  console.log(`Ahora ${a}`);
  a++;
}

// Ejemplo 3
let b = 0;
while (b < 10) {
  if(b === 5){
    console.log('Voy en el 5');
    b++;
    continue;
  }
  console.log(`Ahora ${b}`);
  b++;
}

// Ejemplo 4
const arrayProductos = ['camisa', 'boleto', 'guitarra', 'disco'];
let c = 0;
while (c < arrayProductos.length) {
  console.log(`Tu producto ${arrayProductos[c]} fue agregado!`)
  c++;
}

// Do while (Aunque no cumpla con la condición, se ejecuta 1 vez)
let d = 100;

do{
  console.log(`Número: ${d}`);
  d++;
} while(d < 10);

// *********************** FOR EACH, MAP e Iteradores ********************** //
// Recorrer un arreglo con For Each
const pendientes = ['Tarea', 'Comer', 'Proyecto', 'Aprender javascript'];

pendientes.forEach(function (pendiente, index) {
  console.log(`${index}: ${pendiente}`);
})

// Map para recorrer un arreglo de objetos
const carrito = [
  {id: 1, producto: 'Libro'},
  {id: 2, producto: 'Camisa'},
  {id: 3, producto: 'Guitarra'},
  {id: 4, producto: 'Disco'}
];

const nombreProducto = carrito.map(function (carrito) {
  return carrito.producto;
});

console.log(nombreProducto);

// otra sintaxis diferente de iteración:
const automovil = {
  modelo: 'Camaro',
  motor: 6.1,
  año: 1969,
  marca: 'Chevrolet'
}

for( let auto in automovil){
  console.log(`${auto}: ${automovil[auto]}`);
}