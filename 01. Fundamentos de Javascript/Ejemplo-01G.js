// *********** LA VARIABLE CONST EN OBJETOS Y ARREGLOS ************* //
const numeros = [1,2,3];
// Los arreglos constantes pueden ser modificados 
// El arreglo completo no puede ser reasignado pero los valores individuales si pueden ser modificados. 
numeros[0] = 4;
numeros.push(5);

console.log(numeros);

// ***************** OBJETOS EN JAVASCRIPT ***********************//
// ---- Crear Objeto ----
const persona = {
  nombre: 'Juan',
  apellido: 'Martinez',
  profesion: 'Diseñador gráfico',
  email: 'correo@gmail.com',
  edad: 26,
  musica: ['Rock', 'Blues', 'Heavy Metal'], // arreglo dentro de un objeto
  hogar:{                                   // objeto dentro de otro objeto
    ciudad: 'Santiago',
    pais: 'Chile'
  },
  nacimiento: function () {
    return new Date().getFullYear() - this.edad;
  }
}

console.log(persona);              // Regresa 'Juan'
console.log(persona.email);        // Regresa 'correo@gmail.com'
console.log(persona.musica);       // Regresa el arreglo 'musica'
console.log(persona.nacimiento()); // Regresa año de nacimiento '1992'
console.log(persona.hogar.ciudad); // Regresa 'Santiago' del objeto 'hogar

persona.musica.push('Metal'); 
console.log(persona.musica);

// *************** CREANDO ARREGLOS DE OBJETOS ********************//
const autos = [
  {modelo: 'Mustang', motor: 6.2},
  {modelo: 'Camaro', motor: 6.1},
  {modelo: 'Challenger', motor: 6.3}
];

for(let i = 0; i < autos.length; i++){
  console.log(autos[i]);
}

for(let i = 0; i < autos.length; i++){
  console.log(autos[i].modelo);
}

for(let i = 0; i < autos.length; i++){
  console.log(autos[i].motor);
}

for(let i = 0; i < autos.length; i++){
  console.log(`${autos[i].modelo} ${autos[i].motor}`);
}

autos[0].modelo = 'Audi'; // modificando un elemento de un valor 'const'
console.log(autos);

// ******************* FUNCIONES *********************** 
// ---- Function Declaration ----
function saludar_1() {
  console.log('Hola Miguel!');
}
saludar_1();

function saludar_2(nombre) {
  console.log(`Hola ${nombre}!`);
}
saludar_2('Javier');
saludar_2('Mariana');

function sumar_1(a,b) {
  console.log(a + b);
}
sumar_1(2,3);
sumar_1(6,7);

function sumar_2(a,b) {
  return a + b;
}
let suma = sumar_2(5,10);
console.log(suma);

// ---- Function Expression ----
const sumar = function (a,b) {
  return a + b;
}
console.log(sumar(5,5));

const saludando = function (nombre, edad, profesion) {
  return `Hola ${nombre}, tienes ${edad} y tu profesion es ${profesion}`;
}
console.log(saludando('Juan',22,'Desarrollador'));

// ---- IIFE ----
(function (tecnologia) {
  console.log(`Aprendiendo ${tecnologia}!`);
})('Javascript');

// ---- Métodos de propiedad ----
// Cuando una función se coloca dentro de un objeto
const music = {
  reproducir: function (id_cancion) {
    console.log(`Reproduciendo canción ${id_cancion}`);
  },
  pausar: function () {
    console.log('Pausando la reproducción')
  }
}

music.borrar = function (id_cancion) { // Los métodos también pueden guardarse y crearse fuera del objeto
  console.log(`Borrando la canción con el ID: ${id_cancion}`);
}

music.reproducir(30);
music.pausar();
music.borrar(2);

// *************** MANEJANDO ERRORES CON TRY CATCH *************************//
// función que no existe
try{
  algo();
} catch(error){
  console.log(error);
} finally { // Finally ejecuta con error y sin error.
  console.log('Con error y sin error, se ejecuta de todos modos.')
}

obtenerClientes();

function obtenerClientes() {
  console.log('Descargando...');
  setTimeout(function () { // Tiempo de 3 segundos.
    console.log('Completo');
  }, 3000);
}