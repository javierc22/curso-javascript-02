document.querySelector('#generar-nombre').addEventListener('submit', cargarNombres);

// Llama a Ajax e imprimir resultados:
function cargarNombres(e) {
  e.preventDefault();
  // Leer las variables:
  const origen = document.getElementById('origen');
  const origenSeleccionado = origen.options[origen.selectedIndex].value;

  const genero = document.getElementById('genero');
  const generoSeleccionado = genero.options[genero.selectedIndex].value;

  const cantidad = document.getElementById('numero').value;

  let url= '';
  url += 'https://uinames.com/api/?';
  // Si hay 'origen' seleccionado, agregarlo a la URL
  if (origenSeleccionado !== '') {
    url += `region=${origenSeleccionado}&`;
  }
  // Si hay 'genero' seleccionado, agregarlo a la URL
  if (generoSeleccionado !== '') {
    url += `gender=${generoSeleccionado}&`;
  }
  // Si hay 'cantidad', agregarlo a la URL
  if (cantidad !== '') {
    url += `amount=${cantidad}&`;
  }

  // Conectar con AJAX:
  const xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.onload = function () {
    if (this.status === 200) {
      const nombres = JSON.parse(this.responseText);
      // Generar el HTML
      let htmlNombres = '<h4 class="text-center text-info">Nombres Generados</h4>';
      htmlNombres += '<ul class="lista">';
      // Imprimir cada nombre
      nombres.forEach(function(nombre) {
        htmlNombres += `
          <li class="text-center">${nombre.name}</li>
        `;
      });

      htmlNombres += '</ul>';

      document.getElementById('resultado').innerHTML = htmlNombres;
    }
  }
  // Enviar el request
  xhr.send();
}