// Delegation

document.body.addEventListener('click', eliminarElemento); // Se aplica el evento 'click' en todo el body y se ejecuta la función "eliminarElemento"

function eliminarElemento(e) {
  e.preventDefault();

  if(e.target.classList.contains('borrar-elemento')){ // selecciona los elementos ('objetivo = target') que contengan la clase ingresada
    console.log(e.target.parentElement.parentElement.remove()); // y ejecuta la acción
  }

  if(e.target.classList.contains('agregar-elemento')){ // selecciona los elementos ('objetivo = target') que contengan la clase ingresada
    console.log('Curso Agregado!'); // y ejecuta la acción
  }
}