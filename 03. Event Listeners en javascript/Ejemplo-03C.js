const input_1 = document.querySelector('#input-1');
const input_2 = document.querySelector('#input-2');
const input_3 = document.querySelector('#input-3');
const output_3 = document.querySelector('#output-3');
const input_4 = document.querySelector('#input-4');
const input_5 = document.querySelector('#input-5');
const input_6 = document.querySelector('#input-6');
const input_7 = document.querySelector('#input-7');
const input_8 = document.querySelector('#input-8');

// Key Down
input_1.addEventListener('keydown', obtenerEvento_1);
function obtenerEvento_1(e) {
  console.log(`Evento: ${e.type}`);
  console.log(input_1.value);
}

// Key Up
input_2.addEventListener('keyup', obtenerEvento_2);
function obtenerEvento_2(e) {
  console.log(`Evento: ${e.type}`);
  console.log(input_2.value);
}

// Key press
input_3.addEventListener('keypress', obtenerEvento_3);
function obtenerEvento_3(e) {
  console.log(`Evento: ${e.type}`);
  output_3.innerText = input_3.value;
}

// Focus
input_4.addEventListener('focus', obtenerEvento);

// Blur
input_5.addEventListener('blur', obtenerEvento);

// Cut
input_6.addEventListener('cut', obtenerEvento);

// Copy
input_7.addEventListener('copy', obtenerEvento);

// Paste
input_8.addEventListener('paste', obtenerEvento);

function obtenerEvento(e) {
  console.log(`Evento: ${e.type}`);
}
