// ********************* EVENT LISTENER CLICK ************************ //
// Event Listener click al buscador
// Ex-1:
document.querySelector('#submit-buscador_1').addEventListener('click', function (e) {
  e.preventDefault(); // Le decimos al navegador que no ejecute el "action" incluido en el formulario.
  alert('Buscando cursos...');
});

// Ex-2:
document.querySelector('#submit-buscador_2').addEventListener('click', ejecutarBoton);

function ejecutarBoton(e) {
  e.preventDefault();
  let elemento_1 = e.target; // con 'target' se obtiene información del elemento seleccionado por 'querySelector'
  let elemento_2 = e.target.id;
  let elemento_3 = e.target.className;
  console.log('Ejecutando función ejecutarBoton()')
  console.log(elemento_1);
  console.log(elemento_2);
  console.log(elemento_3);
}

// Ex-3:
document.querySelector("#encabezado-1").addEventListener('click', function (e) {
  e.target.innerText = 'Cambiando encabezado con un click!';
});