const boton_1 = document.querySelector('#boton-1');
const boton_2 = document.querySelector('#boton-2');
const boton_3 = document.querySelector('#boton-3');
const boton_4 = document.querySelector('#boton-4');
const boton_5 = document.querySelector('#boton-5');
const boton_6 = document.querySelector('#boton-6');
const boton_7 = document.querySelector('#boton-7');
const boton_8 = document.querySelector('#boton-8');
const boton_9 = document.querySelector('#boton-9');

// Click
boton_1.addEventListener('click', obtenerEvento);

// Doble Click
boton_2.addEventListener('dblclick', obtenerEvento);

// mouse enter
boton_3.addEventListener('mouseenter', obtenerEvento);

// mouse leave
boton_4.addEventListener('mouseleave', obtenerEvento);

// mouse over
boton_5.addEventListener('mouseover', obtenerEvento);

// mouse out
boton_6.addEventListener('mouseout', obtenerEvento);

// mouse down
boton_7.addEventListener('mousedown', obtenerEvento);
// mouse up
boton_8.addEventListener('mouseup', obtenerEvento);

// mouse move
boton_9.addEventListener('mousemove', function (e) {
  console.log(`Evento: ${e.type}`);
});

function obtenerEvento(e) {
  e.target.innerText = `Evento: ${e.type}`; // 'type' obtiene el tipo de evento que se está ejectutando
}