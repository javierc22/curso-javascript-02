/*
EVENT BUBBLING es un tipo de propagación de eventos, donde el evento se desencadena primero en el elemento objetivo(target) más interno y sucesivamente se desencadena sobre los ancestros (padres) del elemento objetivo, en la misma jerarquía de anidamiento hasta que alcanza el elemento DOM o documento externo más externo (siempre que el controlador esté inicializado). Es una de las formas en que los eventos se manejan en el navegador. Los eventos son acciones realizadas por el usuario, como hacer clic en un botón, cambiar un campo, etc.
*/

const card_1 = document.querySelector('.tarjeta-1');
const infoCard_1 = document.querySelector('.info-tarjeta-1');
const button_1 = document.querySelector('.boton-1');

card_1.addEventListener('click', function (e) {
  console.log('Click en tarjeta 1');  
});

infoCard_1.addEventListener('click', function (e) {
  console.log('Click en información de tarjeta 1');  
});

button_1.addEventListener('click', function (e) {
  console.log('Click en Botón 1');  
});

// Detener la propagación de eventos
const card_2 = document.querySelector('.tarjeta-2');
const infoCard_2 = document.querySelector('.info-tarjeta-2');
const button_2 = document.querySelector('.boton-2');

card_2.addEventListener('click', function (e) {
  console.log('Click en tarjeta 2');
  e.stopPropagation();
});

infoCard_2.addEventListener('click', function (e) {
  console.log('Click en información de tarjeta 2');
  e.stopPropagation();
});

button_2.addEventListener('click', function (e) {
  console.log('Click en Botón 2');
  e.stopPropagation();
});