// Con Function()
let aprendiendo;
aprendiendo = function () {
  console.log('Aprendiendo Javascript (function)');
}
aprendiendo();

// Con Arrow Functions
aprendiendo = () => {
  console.log('Aprendiendo Javascript (Arrow function)');
}
aprendiendo();

// Un Arrow Function con una línea no requiere llaves
aprendiendo = () => console.log('Aprendiendo Javascript (Arrow function de 1 línea)');
aprendiendo();

// Retornar un valor
aprendiendo = () => 'Aprendiendo Javascript (Retornando valor)';
console.log(aprendiendo());

// Retornar un Objeto
aprendiendo = () => ({Aprendiendo: 'Aprendiendo Javascript (Retornando un objeto)'});
console.log(aprendiendo());

// Pasando 1 parámetro
aprendiendo = tecnologia => `Aprendiendo ${tecnologia} (Pasando 1 parámetro)`;
console.log(aprendiendo('Javascript'));

// Pasar parámetros
aprendiendo = (tecnologia_1, tecnologia_2) => `Aprendiendo ${tecnologia_1} y ${tecnologia_2} (Pasando más de un parámetro)`;
console.log(aprendiendo('Javascript', 'ES6'));

// ----------- Ejemplo 1 -------------- //
const productos = ['Disco', 'Camisa', 'Guitarra', 'Piano'];

// Con Function
const letrasProducto_1 = productos.map(function (producto) {
  return producto.length;
});
console.log(letrasProducto_1);

// Con Arrow Function
const letrasProducto_2 = productos.map(producto => producto.length);
console.log(letrasProducto_2);

// Con forEach y function
productos.forEach(function (producto) {
  console.log(producto);
});

// Con ForEach y Arrow Function
productos.forEach( producto => console.log(producto));
