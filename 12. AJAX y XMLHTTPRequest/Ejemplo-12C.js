const cargarPosts = document.querySelector('#cargar').addEventListener('click', cargarAPI);

function cargarAPI() {
  // Crear el Objeto
  const xhr = new XMLHttpRequest();
  // Abrir la conexión
  xhr.open('GET', 'https://jsonplaceholder.typicode.com/posts', true);
  // Cargar y leer datos
  xhr.onload = function () {
    if (this.status === 200) {
      const respuesta = JSON.parse(this.responseText);
    
      let contenido = '';
      respuesta.forEach( function(post) {
        contenido += `
          <h4>${post.title}</h4>
          <p>${post.body}</p>
        `;
      });

      document.getElementById('listado').innerHTML = contenido;
    }
  }

  // Enviar la conexión
  xhr.send();
}