// --------- METODO NUEVO, MÁS ACTUALIZADO A LA FECHA --------------- //
document.getElementById('cargar').addEventListener('click', cargarDatos);

function cargarDatos() {
  // Crear el objeto xmlhttprequest
  const xhr = new XMLHttpRequest();
  // Abrir una conexión
  xhr.open('GET', 'datos.txt', true);
  // Una vez que carga
  xhr.onload = function () {
    // 200: correcto | 403: Prohibido | 404: No encontrado
    if (this.status === 200) {
      document.getElementById('listado').innerHTML = `<h3>${this.responseText}</h3>`;
    }
  }
  // Enviar el Request
  xhr.send();
}

// ----------------- METODO ANTIGUO ------------------------ //
document.getElementById('cargar-2').addEventListener('click', cargarDatos2);
function cargarDatos2() {
  // Crear el objeto xmlhttprequest
  const xhr = new XMLHttpRequest();
  // Abrir una conexión
  xhr.open('GET', 'datos.txt', true);
 
  xhr.onreadystatechange = function () {
    console.log(`Estado: ${this.readyState}`);
    if (this.readyState === 4 && this.status === 200) {
      document.getElementById('listado-2').innerHTML = `<h3>${this.responseText}</h3>`;
    }
  }
  /*
  Ready Status:
  0: No inicializado
  1: Conexión establecida
  2: Recibido
  3: Procesando
  4: Respuesta Lista
  */
  xhr.send();
}