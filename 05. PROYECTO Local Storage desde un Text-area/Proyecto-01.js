localStorage.setItem('Hola', 'Juan');
// Variables
const listaTweets = document.getElementById('lista-tweets');

// Event Listeners
eventListeners();

function eventListeners() {
  // Cuando se envía el formulario:
  document.querySelector('#formulario').addEventListener('submit', agregarTweet);
  // Borrar Tweets:
  listaTweets.addEventListener('click', borrarTweet);
  // Cargar contenido:
  document.addEventListener('DOMContentLoaded', cargarLocalStorage);
}

// Funciones
// Añadir Tweet al formulario:
function agregarTweet(e) {
  e.preventDefault();
  // Leer valor del Text-area:
  const tweet = document.getElementById('tweet').value;
  // Crear Botón de Eliminar:
  const botonBorrar = document.createElement('a');
  botonBorrar.classList = 'borrar-tweet close';
  botonBorrar.innerText = 'X';

  // Crear elemento y añadir contenido a la lista:
  const li = document.createElement('li');
  li.classList = 'row justify-content-between ml-2'
  li.innerText = tweet;
  // Añadir botón Borrar al tweet:
  li.appendChild(botonBorrar);
  // Añadir Tweet a la lista:
  listaTweets.appendChild(li);
  // Añadir a Local Storage
  agregarTweetLocalStorage(tweet);
}

// Eliminar el Tweet del DOM
function borrarTweet(e){
  e.preventDefault();
  if(e.target.classList.contains('borrar-tweet')){
    e.target.parentElement.remove();
    borrarTweetLocalStorage(e.target.parentElement.innerText);
  }
}
// Mostrar datos de Local Storage en la lista
function cargarLocalStorage() {
  let tweets;
  tweets = obtenerTweetsLocalStorage();

  tweets.forEach( function(tweet) {
    // Crear Botón de Eliminar:
    const botonBorrar = document.createElement('a');
    botonBorrar.classList = 'borrar-tweet close';
    botonBorrar.innerText = 'X';

    // Crear elemento y añadir contenido a la lista:
    const li = document.createElement('li');
    li.classList = 'row justify-content-between ml-2'
    li.innerText = tweet;
    // Añadir botón Borrar al tweet:
    li.appendChild(botonBorrar);
    // Añadir Tweet a la lista:
    listaTweets.appendChild(li);
  });
}
// Agregar Tweet a Local Storage
function agregarTweetLocalStorage(tweet) {
  let tweets;
  tweets = obtenerTweetsLocalStorage();
  // Añadir el nuevo tweet
  tweets.push(tweet);
  // Convertir de strings a array para Local Storage
  localStorage.setItem('tweets', JSON.stringify(tweets)); // stringify(); convierte el Json en un valor String
}

// Comprobar que haya elementos en Local Storage, retorna un arreglo
function obtenerTweetsLocalStorage() {
  let tweets;
  // Revisamos los valores de Local Storage:
  if (localStorage.getItem('tweets') === null ) {
    tweets = [];
  } else {
    tweets = JSON.parse(localStorage.getItem('tweets'));
  }
  return tweets;
}

// Eliminar Tweet de Local Storage
function borrarTweetLocalStorage(tweet) {
  let tweets, tweetBorrar;
  // Elimina la X del tweet
  tweetBorrar = tweet.substring(0, tweet.length - 1);
  tweets = obtenerTweetsLocalStorage();
  
  tweets.forEach( function (tweet, index) {
    if(tweetBorrar === tweet){
      tweets.splice(index, 1); // splice(index del arreglo a eliminar, hasta que elementos borrar)
    }
  });

  localStorage.setItem('tweets', JSON.stringify(tweets));
}