// ------ Object Literal ------
const cliente_1 = {
  nombre: 'Juan',
  saldo: '1200',
  tipoCliente: function () {
    let tipo;

    if (this.saldo > 1000) {
      tipo = 'Gold';
    } else if(this.saldo > 500){
      tipo = 'Platinum';
    } else{
      tipo = 'Normal';
    }

    return tipo;
  }
}

console.log(cliente_1.tipoCliente()); 

// ------ Método Alternativo ------
function Cliente_2(nombre, saldo) {
  this.nombre = nombre;
  this.saldo = saldo;
  this.tipoCliente = function () {
    let tipo;

    if (this.saldo > 1000) {
      tipo = 'Gold';
    } else if(this.saldo > 500){
      tipo = 'Platinum';
    } else{
      tipo = 'Normal';
    }

    return tipo;
  }
}

const persona_1 = new Cliente_2('Pedro', 20000);
const persona_2 = new Cliente_2('Pedro', 800);

console.log(persona_1.tipoCliente());
console.log(persona_2.tipoCliente());

// ------ Otros Objetos y Constructores ---------- //
// String
const nombre_1 = 'Pedro';
const nombre_2 = new String('Pedro');
// Números
const numero_1 = 20;
const numero_2 = new Number(20);
// Boolean
const boolean_1 = true;
const boolean_2 = new Boolean(true);
// Funciones
const funcion_1 = function (a, b) {
  return a + b;
}
const funcion_2 = new Function('a','b','return a+b');

console.log(funcion_1(3,5));
console.log(funcion_2(3,4));

// Objetos
const hombre_1 = {
  nombre: 'Juan'
}

const hombre_2 = new Object({nombre: 'Juan'});

// Arreglos
const arreglo_1 = [1,2,3,4,5];
const arreglo_2 = new Array(1,2,3,4,5);