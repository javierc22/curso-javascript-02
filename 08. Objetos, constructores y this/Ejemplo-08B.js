// ------------- PROTOTYPES ----------------- //
function Cliente(nombre, saldo) {
  this.nombre = nombre;
  this.saldo = saldo;
}
// Creando un Prototype:
Cliente.prototype.tipoCliente = function () {
  let tipo;
  if (this.saldo > 1000) {
    tipo = 'Gold';
  } else if(this.saldo > 500){
    tipo = 'Platinum';
  } else{
    tipo = 'Normal';
  }

  return tipo;
}
// Prototype que imprime saldo y nombre
Cliente.prototype.nombreClienteSaldo = function () {
  return `Nombre: ${this.nombre}, tu saldo es de ${this.saldo},
  tipo de cliente: ${this.tipoCliente()}`;
}
// Prototype para retirar saldo
Cliente.prototype.retirarSaldo = function (retiro) {
  return this.saldo -= retiro;
}

const cliente1 = new Cliente('Pedro', 100);
const cliente2 = new Cliente('Karen', 600);
const cliente3 = new Cliente('Miguel', 1200);

console.log(cliente1.nombreClienteSaldo());
console.log(cliente2.nombreClienteSaldo());
console.log(cliente3.nombreClienteSaldo());

cliente2.retirarSaldo(300);
console.log(cliente2.nombreClienteSaldo());


// ------------ HEREDAR PROTOTYPES A OTRO OBJETO --------------- //
function Cliente2(nombre, saldo) {
  this.nombre = nombre;
  this.saldo = saldo;
}

// Prototype que imprime saldo y nombre
Cliente2.prototype.nombreClienteSaldo2 = function () {
  return `Nombre: ${this.nombre}, tu saldo es de ${this.saldo}`;
}

const cliente4 = new Cliente2('Javier', 100);
console.log(cliente4);

// Banca para empresas
function Empresa(nombre, saldo, telefono, tipo) {
  Cliente2.call(this, nombre, saldo); // LLamo la estructura de código de Cliente2.
  this.telefono = telefono;
  this.tipo = tipo;
}

Empresa.prototype = Object.create(Cliente2.prototype); // Heredo los prototypes de Cliente2 para usarlos en 'Empresa'

const empresa = new Empresa('Udemy', 1000000, 53494578, 'Educación');
console.log(empresa.nombreClienteSaldo2());


// ------------ LA FUNCION OBJECT CREATE --------------- //
const Cliente3 = {
  imprimirSaldo: function () {
    return `Hola ${this.nombre}, tu saldo es ${this.saldo}`;
  },
  retirarSaldo: function (retiro) {
    return this.saldo -= retiro;
  }
}
// Crear objeto:
const mary = Object.create(Cliente3);
mary.nombre = 'Mary';
mary.saldo = 1000;

console.log(mary.imprimirSaldo());
mary.retirarSaldo(300);
console.log(mary.imprimirSaldo());