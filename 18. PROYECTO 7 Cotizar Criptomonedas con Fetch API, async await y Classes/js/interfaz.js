class Interfaz{
  constructor(){
    this.init();
  }

  init(){
    this.construirSelect();
  }

  construirSelect(){
    cotizador.obtenerMonedasAPI()
      .then( monedas => {
        // Crear un <select> con las opciones
        const arregloMonedas = monedas.monedas;
        const select = document.getElementById('criptomoneda');
        // Construir <select> desde la REST API
        arregloMonedas.forEach(moneda => {
          // Añadir el ID y el valor, después asignarlo a 'select'
          const option = document.createElement('option');
          option.value = moneda.id;
          option.appendChild(document.createTextNode(moneda.name));
          select.appendChild(option);
        });
      })
  }

  mostrarMensaje(mensaje, clases){
    const div = document.createElement('div');
    div.className = clases;
    div.appendChild(document.createTextNode(mensaje));
    // <div> para mensajes en el HTML
    const divMensaje = document.querySelector('.mensajes');
    // Agregamos el <div> a 'mensajes'
    divMensaje.appendChild(div);
    // Remover alerta después de 3 segundos
    setTimeout(() => {
      document.querySelector('.mensajes div').remove();
    }, 3000);
  }
  // Imprime el resultado de la cotización
  mostrarResultado(resultado, moneda){
    // En caso que existiera un resultado anterior, borrarlo
    const resultadoAnterior = document.querySelector('#resultado > div');
    if (resultadoAnterior) {
      resultadoAnterior.remove();
    }
    // Muestra el Spinner.gif
    this.mostrarSpinner();
    // Construir la etiqueta de precio según la moneda
    const etiquetaMoneda = `price_${moneda}`;
    // Leer el valor del resultado
    const valor = resultado[etiquetaMoneda];
    const monedaUpperCase = moneda.toUpperCase();
    // Obtener ultima actualización. Convierte la hora de UNIX en horas y minutos
    const hora = new Date(resultado.last_updated * 1000); // Se multiplica por 1000 para obtener un valor real
    const horaActualizada = `${hora.getHours()}:${hora.getMinutes()}`; // Obtenemos la hora y los minutos actualizados
    // Construir el Template
    let templateHTML = '';
    templateHTML += `
      <div class="card cyan darken-3">
        <div class="card-content white-text">
          <span class="card-title">Resultado:</span>
          <p>El precio de ${resultado.name} a moneda ${monedaUpperCase} es de: $ ${valor}</p>
          <p>Última hora: ${resultado.percent_change_1h}</p>
          <p>Último día: ${resultado.percent_change_24h}</p>
          <p>Últimos 7 días: ${resultado.percent_change_7d}</p>
          <p>Última actualización: ${horaActualizada}</p>
        </div>
      </div>
    `;
    // Oculta el Spinner.gif y muestra el resultado
    setTimeout(() => {
      // Imprimir el resultado
      document.querySelector('#resultado').innerHTML = templateHTML;
      // Ocultar el spinner.gif
      document.querySelector('.spinner img').remove();
    }, 3000);
  }
  // Muestra un Spinner cuando se cotiza
  mostrarSpinner(){
    const spinnerGif = document.createElement('img');
    spinnerGif.src = 'img/spinner.gif';
    document.querySelector('.spinner').appendChild(spinnerGif);
  }
}