// Constructor para Seguro
class Seguro {
  constructor(marca, anio, tipo){
    this.marca = marca;
    this.anio = anio;
    this.tipo = tipo;
  }

  cotizarSeguro() {
    /* 
    1 = americano 1.15
    2 = asiático 1.05
    3 = europeo 1.35
    */
    let cantidad;
    const base = 2000; // precio base.
  
    switch (this.marca) {
      case '1':
        cantidad = base * 1.15;
        break;
      case '2':
        cantidad = base * 1.05;
        break;
      case '3':
        cantidad = base * 1.35;
        break;
    }
    // Leer el año
    const diferencia = new Date().getFullYear() - this.anio; // calculo de cantidad de años tomando el año actual
    // Cada año de diferencia hay que reducir un 3% el valor del seguro
    cantidad -= ((diferencia * 3) * cantidad) / 100;
    /*
    Si el seguro es 'Básico' se multiplica por 30% más.
    Si el seguro es 'Completo' se multiplica por 50% más.
    */
    if (this.tipo === 'basico') {
      cantidad *= 1.30;
    } else{
      cantidad *= 1.50;
    }
  
    return cantidad;
  }
}


// Todo lo que se muestra
class Interfaz {
  // Mensaje que se imprime en el HTML
  mostrarMensaje(mensaje, tipo) {
    const div = document.createElement('div');

    if (tipo === 'error') {
      div.classList.add('mensaje', 'error');
    } else{
      div.classList.add('mensaje', 'correcto');
    }

    div.innerHTML = `${mensaje}`;
    formulario.insertBefore(div, document.querySelector('.form-group'));

    setTimeout(function() {
      document.querySelector('.mensaje').remove();
    }, 3000);
  }
  // Imprime el resultado de la cotización
  mostrarResultado(seguro, total) {
    const resultado = document.getElementById('resultado');
    let marca;

    switch (seguro.marca) {
      case '1':
        marca = 'Americano';
        break;
      case '2':
        marca = 'Asiático';
        break;
      case '3':
        marca = 'Europeo';
        break;
    }
    // Crear un <div>
    const div = document.createElement('div');
    // Insertar la información
    div.innerHTML = `
      <p class="header">Tu resumen:</p>
      <p>Marca: ${marca}.</p>
      <p>Año: ${seguro.anio}.</p>
      <p>Tipo: ${seguro.tipo}.</p>
      <p>Total: $ ${total}</p>
    `;

    const spinner = document.querySelector('#cargando img');
    spinner.style.display = 'block';
    setTimeout(function() {
      spinner.style.display = 'none';
      resultado.appendChild(div);
    }, 3000);
  }
}

// EventListener
const formulario = document.getElementById('cotizar-seguro');

formulario.addEventListener('submit', function (e) {
  e.preventDefault();
  // Leer la 'marca' seleccionada en el <select>
  const marca = document.getElementById('marca');
  const marcaSeleccionada = marca.options[marca.selectedIndex].value;
  // Leer el 'año' seleccionada en el <select>
  const anio = document.getElementById('anio');
  const anioSeleccionado = anio.options[anio.selectedIndex].value;
  // Leer el valor del 'radio button'
  const tipo = document.querySelector('input[name="tipo"]:checked').value;

  // Crear instancia de 'Interfaz'
  const interfaz = new Interfaz();
  // Revisamos que los campos no estén vacíos
  if (marcaSeleccionada === '' || anioSeleccionado === '' || tipo === '') {
    // Interfaz imprimiendo un error
    interfaz.mostrarMensaje('Faltan datos, revisar el formulario y prueba de nuevo', 'error');
  } else{
    // Limpiar resultados anteriores
    const resultados = document.querySelector("#resultado div");
    if (resultados != null) {
      resultados.remove();
    }
    // Instanciar 'Seguro' y mostrar intrfaz
    const seguro = new Seguro(marcaSeleccionada, anioSeleccionado, tipo);
    // Cotizar el seguro
    const cantidad = seguro.cotizarSeguro();
    // Mostrar el resultado
    interfaz.mostrarResultado(seguro, cantidad);
    interfaz.mostrarMensaje('Cotizando...', 'correcto');
  }
});

const max = new Date().getFullYear(), // Obtenemos el año actual
      min = max - 20;

const selectYear = document.getElementById('anio');
for (let i = max; i >= min; i--) {
  let option = document.createElement('option');
  option.value = i;
  option.innerHTML = i;
  selectYear.appendChild(option);
}