// --------------- HERENCIAS EN CLASES ------------------- //
class Cliente2 {
  constructor(nombre, saldo){
    this.nombre = nombre;
    this.saldo = saldo;
  }

  imprimirSaldo(){
    return `Hola ${this.nombre}, tu saldo es de: ${this.saldo}`;
  }

  // Método estático:
  static bienvenida(){
    return `Bienvenido al Cajero2!`;
  }
}
// Creando una clase que hereda de la clase 'Cliente2'
class Empresa extends Cliente2 {
  constructor(nombre, saldo, telefono, tipo){
    // Va hacia el constructor padre (class Cliente2):
    super(nombre, saldo);
    // Como no existen en el constructor padre, se deben declarar:
    this.telefono = telefono;
    this.tipo = tipo;
  }

  // Método estático:
  static bienvenida(){
    return `Bienvenido al Cajero para empresas!`;
  }
}

const empresa = new Empresa('Empresa1', 10000, 53457852, 'Construcción');

console.log(Cliente2.bienvenida());
console.log(Empresa.bienvenida());
console.log(empresa.imprimirSaldo());