// --------------- CREANDO UNA CLASE ------------------- //
class Cliente {
  constructor(nombre, apellido, saldo){
    this.nombre = nombre;
    this.apellido = apellido;
    this.saldo = saldo;
  }

  imprimirSaldo(){
    return `Hola ${this.nombre}, tu saldo es de: ${this.saldo}`;
  }

  tipoCliente(){
    let tipo;
    if (this.saldo > 10000) {
      tipo = 'Gold';
    } else if(this.saldo > 5000){
      tipo = 'Platinum';
    } else{
      tipo = 'Normal';
    }

    return tipo;
  }

  retirarSaldo(retiro){
    return this.saldo -= retiro;
  }

  // Método estático:
  static bienvenida(){
    return `Bienvenido al Cajero!`;
  }
}

console.log(Cliente.bienvenida());
const maria = new Cliente('María', 'Perez', 11000);
console.log(maria.imprimirSaldo());
console.log(maria.tipoCliente());
maria.retirarSaldo(3000);
console.log(maria.imprimirSaldo());
console.log(maria.tipoCliente());