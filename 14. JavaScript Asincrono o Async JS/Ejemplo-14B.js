// ------------- Promises: Resolve ----------------//
// Ejemplo 1:
const esperando = new Promise(function (resolve, reject) {
  setTimeout(function() {
    resolve('Se ejecutó sin problemas');
  }, 5000);
});

esperando.then(function (mensaje) {
  console.log(mensaje);
});

// Ejemplo 2:
const aplicarDescuento = new Promise(function (resolve, reject) {
  const descuento = false;
  if (descuento) {
    resolve('Descuento Aplicado');
  } else {
    reject('No se puede aplicar el descuento');
  }
});

aplicarDescuento.then(function (resultado) {
  console.log(resultado); 
}).catch(function (error) {
  console.log(error);
});
