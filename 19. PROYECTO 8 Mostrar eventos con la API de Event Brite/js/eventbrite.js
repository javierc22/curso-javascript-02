class EventBrite{
  constructor(){
    this.token_auth = 'W4XZCKGLUY2VYZK7U3AW';
    this.ordenar = 'date';
  }

  // Mostrar resultados de la búsqueda
  async obtenerEventos(evento, categoria){
    const respuestaEvento = await fetch(
      `https://www.eventbriteapi.com/v3/events/search/?q=${evento}&sort_by=${this.ordenar}&categories=${categoria}&token=${this.token_auth}`);
    
      // Esperar la respuesa del evento y devolverlo como JSON
      const eventos = await respuestaEvento.json();
      // Devolvemos el resultado
      return {
        eventos
      }
  }

  // Obtiene las categorias en init()
  async obtenerCategorias(){
    // Consultar las categorias a las REST API de Event Brite
    const respuestaCategorias = await fetch(`https://www.eventbriteapi.com/v3/categories/?token=${this.token_auth}`);
    // Esperar la respuesta de las categorias y devolver un Json
    const categorias = await respuestaCategorias.json();
    // Devolvemos el resultado
    return {
      categorias
    }
  }
}