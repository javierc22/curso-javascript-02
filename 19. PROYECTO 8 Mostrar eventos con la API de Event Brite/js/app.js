// Instanciar ambas clases:
const eventBrite = new EventBrite();
const ui = new Interfaz();

// Listener al buscador
document.getElementById('buscarBtn').addEventListener('click', (e) =>{
  e.preventDefault();

  // Leer el texto del <input> al buscar
  const textoBuscador = document.getElementById('evento').value;

  // Leer el <select>
  const categorias = document.getElementById('listado-categorias');
  const categoriaSeleccionada = categorias.options[categorias.selectedIndex].value;

  // Revisar que haya algo escrito en el buscador
  if (textoBuscador != '') {
    // Cuando si existe un búsqueda
    eventBrite.obtenerEventos(textoBuscador, categoriaSeleccionada)
      .then(eventos => {
        if (eventos.eventos.events.length > 0) {
          // Si hay eventos, mostrar resultado
          ui.limpiarResultados();
          ui.mostrarEventos(eventos.eventos);
        } else {
          // Si no hay eventos, enviar una alerta
          ui.mostrarMensaje('No hay resultados', 'alert alert-danger mt-4');
        }
      })
  } else {
    // Mostrar mensaje para que el usuario escriba algo
    ui.mostrarMensaje('Escribe algo en el buscador', 'alert alert-danger mt-4');
  }
})
